#include<stdio.h>
#include<stdlib.h>
#include<time.h>

int **arr,n,bs;
int *minrow;
int *mincol;
int *minbox;
int *finalize;
int minr,minc,minb;

void cleanup() // Deallocation
{
	int i;

	if (arr!=NULL)
    {
        for(i=0;i<n;i++)
            if (arr[i]!=NULL)
                free(arr[i]);

        free (arr);
    }

	if (minrow!=NULL)
        free (minrow);
    if (mincol!=NULL)
        free (mincol);
    if (minbox != NULL)
        free (minbox);
    if (finalize!=NULL)
        free (finalize);
}

void Display() // Display the sudoku matrix
{
    int i,j;
    for(i=0;i<n;i++)
    {
        printf("\n");
        for(j=0;j<n;j++)
        {
            printf (" ");
            if(arr[i][j]!=0)
               printf("%2d",arr[i][j]);
            else
               printf(" -");
        }
    }
}


int checkrow(int l,int *i1, int *j1)
{
// checks for duplicate elements in a row
// returns 0 when no duplicate is found
// else returns the column number of second duplicate element
// i1,j1 are outputs conatining the coords of first duplicate element

	int i,j,k;
	if(l==0)    // l==0 means check all rows
	{
	for(i=0;i<n;i++)
		for(j=0;j<n;j++)
			for(k=j+1;k<n;k++)
				if(arr[i][j]!=0 && arr[i][j]==arr[i][k])
                {
                    *i1=i;
                    *j1=j;
                    return k;
                }

	}
	else    // l!=0 means check only row 'l'
	{
		for(j=0;j<n;j++)
			for(k=j+1;k<n;k++)
				if(arr[l][j]!=0 && arr[l][j]==arr[l][k])
                {
                    *i1=l;
                    *j1=j;
                    return k;
                }

	}
	return 0;
}


int checkcolumn(int l,int *i1, int *j1)
{
// checks for duplicate elements in a column
// returns 0 when no duplicate is found
// else returns the row number of second duplicate element
// i1,j1 are outputs conatining the coords of first duplicate element

	int i,j,k;
	if(l==0)        // l==0 means check all coloumns
	{
	for(i=0;i<n;i++)
		for(j=0;j<n;j++)
			for(k=j+1;k<n;k++)
				if(arr[j][i]!=0 && arr[j][i]==arr[k][i])
                {
                    *i1=j;
                    *j1=i;
                    return k;
                }

	}
	else    // l!=0 means check only coloum 'l'
	{
	for(j=0;j<n;j++)
		for(k=j+1;k<n;k++)
			if(arr[j][l]!=0 && arr[j][l]==arr[k][l])
            {
                *i1=j;
                *j1=l;
                return k;
            }
	}
	return 0;
}

inline int box(int b,int i1,int j1)
// returns element at position (i1,j1) inside box 'b'
// Note: (i1,j1) are relative coords to the first element
//       in the box.
{
    int i,j;
    i=((b/bs)*bs)+i1;
    j=((b%bs)*bs)+j1;
    return arr[i][j];
}

inline void SetBoxElement(int b,int i1,int j1,int e)
// sets value 'e' to cell at position (i1,j1) inside box 'b'
// Note: (i1,j1) are relative coords to the first element
//       in the box.
{
    int i,j;
    i=((b/bs)*bs)+i1;
    j=((b%bs)*bs)+j1;
    arr[i][j]=e;
}

int checkbox(int b,int *i1,int *j1, int *i2, int *j2)
{
// Checks the box 'b' for duplicate elements.
// Returns 0 for no duplicate
// Returns the box number if duplicate is found when checking all boxes
// Returns the duplicate element when checking a particular box
// OUTPUT: (i1,j1) and (i2,j2) are actual coords (not relative coords)
//  of the two duplicate elements

    int i,j,k,t;
    if (b==0)   // b==0 means check all boxes
    {
        for(i=b;i<n;i++)
        {
            for(j=0;j<n;j++)
            {
                for(k=j+1;k<n;k++)
                 {
                    t=box(i,j/bs,j%bs);
                    if((t!=0)&&(t==box(i,k/bs,k%bs)))
                    {
                        *i1=((i/bs)*bs)+(j/bs);
                        *j1=((i%bs)*bs)+(j%bs);
                        *i2=((i/bs)*bs)+(k/bs);
                        *j2=((i%bs)*bs)+(k%bs);
                        return i;
                    }
                }
            }
        }
    }
    else
    {
            i=b;
            for(j=0;j<n;j++)
            {
                for(k=j+1;k<n;k++)
                 {
                    t=box(i,j/bs,j%bs);
                    if((t!=0)&&(t==box(i,k/bs,k%bs)))
                    {
                        *i1=((i/bs)*bs)+(j/bs);
                        *j1=((i%bs)*bs)+(j%bs);
                        *i2=((i/bs)*bs)+(k/bs);
                        *j2=((i%bs)*bs)+(k%bs);

                        return t;
                    }

                }
            }
    }
    return 0;
}

int sqrt(int l)
{
    int i=1;
    while (1)
    {
        if (i*i>=l)
            return i;
        ++i;
    }
}

int input()
{
	int i,j,k,num,flag;
	do
    {
	printf("\n\t Enter the number of digits (1 to n |n=4,9,16,...): ");

	scanf("%d",&n);
    bs=sqrt(n);
    if((bs*bs)!=n)
    {
        printf("Error! Value of n should be a square number. e.g 4,9,16....");
        continue;
    }
    break;
    }while(1);

	arr = malloc(n * sizeof(int *));
	if(arr == NULL)
	{
		printf("out of memory\n");
		return 1;
	}
	for(i = 0; i < n; i++)
	{
		arr[i] = malloc(n * sizeof(int));
		if(arr[i] == NULL)
        {
			printf("out of memory\n");
			for (j=0;j<i;j++)
                free(arr[i]);
            free (arr);
			return 1;
		}
	}
    minbox=malloc((n)*sizeof(int));
    minrow=malloc((n)*sizeof(int));
    mincol=malloc((n)*sizeof(int));
    finalize=malloc((n*n)*sizeof(int));

    if (minbox==NULL || minrow==NULL || mincol==NULL ||  finalize==NULL)
        return 1;

    printf("\n You can also enter all elements sequentially all at once, seperated by spaces.\n");
    while (1)
    {
        for(i = 0; i < n; i++)
        {
         printf("\n\tEnter 0 for blank cells\n");
         printf("\n\tEnter elements of row %d:",i+1);
         flag=0;
         for(j=0;j<n;j++)
         {
            printf("\n\tEnter value for [%d][%d] : ",i+1,j+1);
            scanf("%d",&num);
            if(num==0)
            {
                arr[i][j]=0;
                continue;
            }
            if(num < 1 || num > n)
            {
                printf("\tWrong input..Please, try again\n");
                j--;
                continue;
            }
            for(k=0;k<j;k++)
            {
                if(arr[i][k]==num)
                {
                    printf("\tDuplicate input..Please, try again\n");
                    break;
                }
            }
            if(k<j)
            {
                i=-1;
                printf ("---Restart from Row 0---\n");
                break;
            }
            for(k=0;k<i;k++)
            {

                if(arr[k][j]==num)
                {
                    printf("\tDuplicate input..Please, try again\n");
                    break;
                }
            }

            if(k<i)
            {
                i=-1;
                printf ("---Restart from Row 0---\n");
                break;
            }
            arr[i][j]=num;
          }
        }
        flag=0;

        for(i=0;i<n;i++)
        {
            for(j=0;j<n;j++)
            {
                for(k=j+1;k<n;k++)
                 {
                    if((box(i,j/bs,j%bs)!=0)&&(box(i,j/bs,j%bs)==box(i,k/bs,k%bs)))
                    {
                        printf("\tDuplicate input '%d' for box number %d..Please, try again\n",box(i,j/bs,j%bs),i);
                        flag=1;
                        break;
                    }

                }
                if(flag)
                    break;
            }
                if(flag)
                    break;
        }

        if (flag)
        {
            Display();
        }
        else
            break;
    }

    Display();

    return 0;
}

void UpdateRemaining()
{
    // Purpose of this function is to maintain a list of the number of
    // remainging elements to fill in a row/col/box.
    // It will also set the row/col/box with minimum values to fill,
    // in minr,minc and minb variables

    int i,j;

    for (i=0;i<n;i++)
    {
        minrow[i]=mincol[i]=minbox[i]=0;
    }


    for (i=0;i<n;i++)
    {
        for (j=0;j<n;j++)
        if (arr[i][j]==0)
        {
            minrow[i]++;
            mincol[j]++;
            minbox[((i/bs)*bs + (j/bs))]++;
        }
    }

    for (i=0;i<n;i++)
    {
        if (minrow[i]==0) minrow[i]=999;
        if (mincol[i]==0) mincol[i]=999;
        if (minbox[i]==0) minbox[i]=999;
    }

    // Find minimum values to fill in row/col/box
    minr=minc=minb=0;

    for (j=1;j<n;j++)
    {
        if (minrow[minr]!=0 && minrow[j]<minrow[minr])
            minr=j;
        if (mincol[minc]!=0 && mincol[j]<mincol[minc])
            minc=j;
        if (minbox[minb]!=0 && minbox[j]<minbox[minb])
            minb=j;
    }
}

void FillLastCell()
{
    // If only one value is left to fill in a row/col/box, this function will fill it.
    // This will be done in a loop.
    // So in some simple case such as a 4x4 sudoku like this (1 2 3 0 4 0 1 0 2 0 0 3 0 4 0 1),
    // no backtracking is required as this function will fill all the values in a loop.

	int i,pos,*seq=malloc((n+1)*sizeof(int)),t;

	while (minrow[minr]==1 || mincol[minc]==1 || minbox[minb]==1)
	{
		if (minr >=0 && minr<n && minrow[minr]==1)
		{
			for (i=0;i<n+1;i++)
				seq[i]=0;
			for (i=0;i<n;i++)
			{
				seq[arr[minr][i]]++;
				if (arr[minr][i]==0 && seq[0]==1)
					pos=i;
			}

			for (i=0;seq[i];i++);
			arr[minr][pos]=i;
			minrow[minr]=999;
		}

		else if (minc>=0 && minc<n && mincol[minc]==1)
		{
			for (i=0;i<n+1;i++)
				seq[i]=0;
			for (i=0;i<n;i++)
			{
				seq[arr[i][minc]]++;
				if (arr[i][minc]==0 && seq[0]==1)
					pos=i;
			}

			for (i=0;seq[i];i++);
			arr[pos][minc]=i;
			mincol[minc]=999;
		}

		else if (minc>=0 && minc<n && minbox[minb]==1)
		{
			for (i=0;i<n+1;i++)
				seq[i]=0;
			for (i=0;i<n;i++)
			{
			    t=box(minb,i/bs,i%bs);
				seq[t]++;
				if (t==0 && seq[0]==1)
					pos=i;
			}

			for (i=0;seq[i];i++);
			SetBoxElement(minb,pos/bs,pos%bs,i);
			minbox[minb]=999;
		}

		UpdateRemaining();
	}
}


int firstfit(int pos){
    //
    // This is the backtracking algorithm.
    //
    int val,i1,j1,i2,j2,r=pos/n,c=pos%n,flag=0;

    if (pos>=(n*n))
        return 1;

    if ((finalize[r*n+c]==1)) // The value in (r,c) is finalized, no need to test further
            return firstfit(pos+1);


    if (arr[r][c]!=0)
    {   // Optimisation: If the value is not blank and not finalized, then it means some
        // functions such as FillLastCell has already selected a value.
        // So test the value directly.

        if (checkrow(r,&i1,&j1)==0 && checkcolumn(c,&i1,&j1)==0 && checkbox(((r/bs)*bs)+(c/bs),&i1,&j1,&i2,&j2)==0 )
            {
                     if (firstfit(pos+1)>0)
                     {
                        UpdateRemaining();
                        FillLastCell();
                        return 1;
                     }
                     else
                        flag=1;
            }
        else
        {
            flag=1; // The value selected by FillLastCell has failed. So set the flag
                    // to test all values (brute-force)
            arr[r][c]=0;
        }
    }
    else
        flag=1;

    if (flag)   // Do brute-force search
    for (val=1;val<=n;val++)
    {
        arr[r][c]=val;
        // Optimisation: If one function in the following if statement fails
        // then the C compiler will not call other functions.
        if (checkrow(r,&i1,&j1)==0 && checkcolumn(c,&i1,&j1)==0 && checkbox(((r/bs)*bs)+(c/bs),&i1,&j1,&i2,&j2)==0 )
            {
                     if (firstfit(pos+1)>0)
                     {
                        UpdateRemaining(); // One value has succeede.
                        FillLastCell(); // FillLastCell will check for last values to fill
                        return 1;
                     }
            }
        else
        {
            arr[r][c]=0;
        }
    }
    arr[r][c]=0;
    return 0;
}

int main()
{
    int i1,j1,i2,j2,i,j;
	double t;
	time_t start,end;

    printf("\t\t****** Sudoku Puzzle ******\n\n");

    if (input()!=0){
        printf ("\nUnrecoverable error!");
        cleanup();
        return -1;
    }

    start=clock();

    UpdateRemaining();

    FillLastCell();

    // Finalisation of values. This is a binary array which will correspond to sudoku matrix.
    // It will store 1 for values with 100% confidence. e.g. the values which the user has entered
    // or the values entered by the first call to FillLastCell(). For all other values, it will store 0
    if (checkrow(0,&i1,&j1)==0 &&
        checkcolumn(0,&i1,&j1)==0 &&
        checkbox(0,&i1,&j1,&i2,&j2)==0){
            // Finalize
            for (i=0;i<n;i++)
                for (j=0;j<n;j++)
                    if (arr[i][j]!=0)
                        finalize[i*n+j]=1;
                    else
                        finalize[i*n+j]=0;
        }
    else{
            // This shouldnt happen as first call to FilllastCell() shouldn't fail!
            printf ("\n\nERROR: Control shouldn't be here!!!!!!");
            cleanup();
            return -1;
    }

    //Optimisation: Go to the first blank cell, instead of brute-forcing from first cell
    for (i1=0;finalize[i1]!=1;i1++);

    firstfit(i1); // Call the backtracking algorithm

    end=clock();
    t=(float)(end-start)/CLOCKS_PER_SEC;

    printf ("\n\t ******** Solution: ******** \n");
    Display();

	printf("\n Time Required: %5.3fs\n",t);

    cleanup();
	return 0;
}
